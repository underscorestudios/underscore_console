import React from "react";
import { Table, Button } from "bloomer";

export default class Scene extends React.Component {
  state = {
    isActive: false,
  };

  componentDidMount() {
    if (!this.props.sceneData) {
      this.props.requestSceneRefresh();
    }
  }

  render() {
    return (
      <div>
        <Button onClick={() => this.props.requestSceneRefresh()}>
          Refresh
        </Button>
        {this.props.sceneData && (
          <div style={{ marginTop: "10px" }}>
            <Table isBordered isStriped isNarrow>
              <tbody>
                {Object.entries(this.props.sceneData).map(([key, value]) => (
                  <tr key={key}>
                    <td>
                      <b>{key}</b>
                    </td>
                    <td>
                      {typeof value == "boolean" ? value.toString() : value}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </div>
    );
  }
}
