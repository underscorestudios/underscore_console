import React from "react";
import { Input } from "bloomer";
import moment from "moment";
import { useEffect } from "react";
import { useRef } from "react";

const Console = (props) => {
  const scroll = useRef(null);

  useEffect(() => {
    const scrollHeight = scroll.current.scrollHeight;
    const height = scroll.current.clientHeight;
    const maxScrollTop = scrollHeight - height;
    scroll.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  });

  return (
    <div
      className="console-inside"
      style={{ display: "flex", flexDirection: "column" }}
    >
      <div
        ref={scroll}
        style={{ height: "100%", marginBottom: "50px", overflowY: "scroll" }}
      >
        {props.logs.map((log) => (
          <p
            key={log.key}
            style={{
              width: "100%",
              borderBottom: "solid 1px #0000001f",
              backgroundColor: log.type === "error" ? "#ff000021" : "unset",
            }}
          >
            <b>[{moment(log.timestamp).format("HH:mm:SSS")}] </b>
            {log.text}
          </p>
        ))}
      </div>
      <Input
        disabled
        style={{ position: "absolute", bottom: "10px", marginTop: "15px" }}
        type="text"
        placeholder="Command Field"
      />
    </div>
  );
};

export default Console;
