import React from "react";
import {
  Hero,
  Tab,
  HeroFooter,
  Tabs,
  TabList,
  HeroBody,
  Container,
  Title,
} from "bloomer";
import { Link, withRouter } from "react-router-dom";

const HeroTabs = [
  { name: "Overview", to: "/" },
  { name: "Console", to: "/console" },
  { name: "Performance", to: "/performance" },
  { name: "Scene", to: "/scene" },
];

const MainHero = (props) => {
  return (
    <Hero
      style={{ transition: "background-color 150ms ease-in-out" }}
      isColor={
        props.status === 0 ? "primary" : props.status === 1 ? "info" : "danger"
      }
      isSize="small"
    >
      <HeroBody style={{ marginTop: "30px" }}>
        <Container hasTextAlign="centered">
          <Title>
            {props.status === 0
              ? "Waiting"
              : props.status === 1
              ? "Connected"
              : "Disconnected"}
          </Title>
        </Container>
      </HeroBody>
      <HeroFooter>
        <Tabs isBoxed>
          <Container>
            <TabList>
              {HeroTabs.map((tab) => (
                <Tab
                  key={tab.name}
                  isActive={props.location.pathname === tab.to}
                >
                  <Link to={tab.to}>{tab.name}</Link>
                </Tab>
              ))}
            </TabList>
          </Container>
        </Tabs>
      </HeroFooter>
    </Hero>
  );
};

export default withRouter(MainHero);
