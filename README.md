# UNDERSCORE_CONSOLE
![alt text](public/logo192.png "Logo")

This is the front-end for our websocket game console project.<br/>
You can view the always up-to-date build at [https://console.underscorestudios.net/](https://console.underscorestudios.net/).

## Running and Building

Open the project directory and run: `yarn` or `npm install` if you don't have yarn.

### Running
Run `yarn start` to start the development server.

### Building

Run `yarn build` to create a production build. It will build into a `build` folder.
