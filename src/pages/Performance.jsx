import React from "react";
import { Title } from "bloomer/lib/elements/Title";

const getFpsColor = (biggestFPS, framerate) => {
  var prc = framerate / biggestFPS;
  return `rgb(${prc > 0.5 ? 0 : (1 - prc) * 255}, ${
    prc < 0.5 ? 0 : prc * 230
  }, 0)`;
};

const Performance = (props) => {
  if (props.fpsGraph.length === 0) return null;
  return (
    <div>
      <Title>FPS: {props.fpsGraph[0].value}</Title>
      <hr />
      <Title>FPS Graph</Title>
      <div
        style={{
          width: "100%",
          height: "200px",
          display: "flex",
          flexDirection: "row-reverse",
          justifyContent: "space-evenly",
          alignItems: "flex-end",
        }}
      >
        {props.fpsGraph.map((framerate) => (
          <div
            key={framerate.key}
            style={{
              transition: "height 150ms ease-in-out",
              color:
                framerate.value / props.biggestFramerate < 0.05
                  ? "black"
                  : "white",
              backgroundColor: getFpsColor(
                props.biggestFramerate,
                framerate.value
              ),
              height: (framerate.value / props.biggestFramerate) * 100 + "%",
              flex: "1",
            }}
          >
            <div style={{ textAlign: "center" }}>{framerate.value}</div>
          </div>
        ))}
      </div>
      <hr />
    </div>
  );
};

export default Performance;
