import React from "react";
import "./App.css";
import { Control, Input } from "bloomer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Overview from "./pages/Overview";
import Console from "./pages/Console";
import MainHero from "./components/MainHero";
import Performance from "./pages/Performance";
import Modal from "./components/Modal";
import Scene from "./pages/Scene";

let ws;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.modal1 = React.createRef();
  }

  state = {
    connectAddress: "127.0.0.1:7851",
    status: 0, // 0 - Waiting
    // 1 - Connected
    // 2 - Disconnected
    // No, I won't use some fake enums
    gameData: null,
    sceneData: null,
    fpsGraph: [],
    biggestFramerate: 0,
    logs: [],
  };

  setFpsGraphSize = (size) => {
    let fpsGraph = [];
    for (var i = 0; i < size; i++) {
      fpsGraph.push({ value: 0, key: i });
    }
    this.setState({ fpsGraph });
  };

  requestSceneRefresh = () => {
    if (!ws) {
      this.connect(this.requestSceneRefresh);
      return;
    }
    if (this.state.status !== 1) return;
    ws.send(JSON.stringify({ command: "get-scene" }));
  };

  connect = (onConnected) => {
    this.setState({ status: 0 });
    if (ws) ws.close();
    ws = new WebSocket(`ws://${this.state.connectAddress}`);
    ws.onopen = (event) => {
      ws.send(JSON.stringify({ command: "get-game-data" }));
      this.setState({ status: 1 });
      if (onConnected) onConnected();
    };
    ws.onerror = (event) => {
      this.setState({ status: 2 });
    };
    ws.onclose = (event) => {
      if (this.state.status !== 1) this.modal1.current.enableModal();
      this.setState({ status: 2 });
      console.log("Socket closed");
    };

    ws.onmessage = (event) => {
      const json = JSON.parse(event.data);
      switch (json.status) {
        case "game-data":
          this.setState({ gameData: json.data });
          break;
        case "scene-data":
          this.setState({ sceneData: json.data });
          break;
        case "debug-log":
          const logs = this.state.logs;
          let data = json.data;
          data.key = logs.length;
          logs.push(data);
          this.setState({ logs });
          break;
        case "framerate":
          let graph = this.state.fpsGraph;
          graph.unshift({
            value: json.data,
            key: graph[graph.length - 1].key + 1,
          });
          graph.pop();
          let biggestFramerate = 0;
          graph.forEach(({ value: framerate }) => {
            if (framerate > biggestFramerate) {
              biggestFramerate = framerate;
            }
          });
          this.setState({
            fpsGraph: graph,
            biggestFramerate: biggestFramerate,
          });
          break;
        default:
          break;
      }
    };
  };

  componentDidMount() {
    if (!ws) this.connect();
    this.setFpsGraphSize(20);

    global.setFpsGraphSize = this.setFpsGraphSize;
  }

  render() {
    return (
      <>
        <Modal
          onSubmit={this.connect}
          ref={this.modal1}
          title="Failed to connect"
        >
          <p>We couldn't find a supported application!</p>
          <p>
            If you are sure that a supported application is running, you might
            have to press <b>Tilde (~)</b> in-game to enable it
          </p>
          <hr />
          <p>Or connect to it remotely, by typing its address below.</p>
          <Control>
            <Input
              value={this.state.connectAddress}
              onChange={(event) =>
                this.setState({ connectAddress: event.target.value })
              }
              type="text"
              placeholder="Address"
            />
          </Control>
        </Modal>
        <Router>
          <MainHero status={this.state.status}></MainHero>
          <div
            style={{
              padding: "10px",
              height: "auto",
              position: "relative",
              marginBottom: "0px",
            }}
          >
            <Switch>
              <Route exact path="/">
                <Overview gameData={this.state.gameData}></Overview>
              </Route>
              <Route path="/console">
                <Console logs={this.state.logs}></Console>
              </Route>
              <Route path="/performance">
                <Performance
                  fpsGraph={this.state.fpsGraph}
                  biggestFramerate={this.state.biggestFramerate}
                ></Performance>
              </Route>
              <Route path="/scene">
                <Scene
                  requestSceneRefresh={this.requestSceneRefresh}
                  sceneData={this.state.sceneData}
                ></Scene>
              </Route>
            </Switch>
          </div>
        </Router>
      </>
    );
  }
}

export default App;
