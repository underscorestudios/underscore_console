import React from "react";
import {
  Modal as BulmaModal,
  ModalBackground,
  ModalCard,
  ModalCardHeader,
  ModalCardTitle,
  ModalCardBody,
  ModalCardFooter,
  Button,
} from "bloomer";

export default class Modal extends React.Component {
  state = {
    isActive: false,
  };

  toggleModal = () => {
    this.setState({ isActive: !this.state.isActive });
  };

  enableModal = () => {
    this.setState({ isActive: true });
  };

  render() {
    return (
      <BulmaModal isActive={this.state.isActive}>
        <ModalBackground />
        <ModalCard>
          <ModalCardHeader>
            <ModalCardTitle>{this.props.title || "Title"}</ModalCardTitle>
          </ModalCardHeader>
          <ModalCardBody>{this.props.children}</ModalCardBody>
          <ModalCardFooter>
            <Button
              onClick={() => {
                this.toggleModal();
                this.props.onSubmit();
              }}
              isColor="success"
            >
              Connect
            </Button>
          </ModalCardFooter>
        </ModalCard>
      </BulmaModal>
      //   <ModalWrapper active={this.state.isActive}>
      //     <ModalBackground
      //       active={this.state.isActive}
      //       onClick={this.toggleModal}
      //     ></ModalBackground>
      //     <ModalContentWrapper active={this.state.isActive}>
      //       <ModalHead>
      //         <h2>{this.props.title || "Title"}</h2>
      //       </ModalHead>
      //       <ModalContent>{this.props.children}</ModalContent>
      //       <ModalFooter>
      //         <PrimaryButton onClick={this.toggleModal} noMargins last>
      //           done
      //         </PrimaryButton>
      //       </ModalFooter>
      //     </ModalContentWrapper>
      //   </ModalWrapper>
    );
  }
}
