import React from "react";
import { Table } from "bloomer";

const Overview = (props) => {
  if (!props.gameData) return null;
  return (
    <Table isBordered isStriped isNarrow>
      <tbody>
        {Object.entries(props.gameData).map(([key, value]) => (
          <tr key={key}>
            <td>
              {" "}
              <b>{key}</b>
            </td>
            <td>{typeof value == "boolean" ? value.toString() : value}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default Overview;
